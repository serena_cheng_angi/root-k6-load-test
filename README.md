# K6 Load test 
1. [Install k6](https://k6.io/docs/getting-started/installation/)

2. [Convert to js](https://k6.io/docs/test-authoring/recording-a-session/har-converter/)

3. [References for test options](https://k6.io/docs/using-k6/k6-options/reference)

To run the test. Default option is set up 200 virtual users in 10s. 
```
k6 run halandingpage.js
```