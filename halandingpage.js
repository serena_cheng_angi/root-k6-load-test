// Creator: WebInspector 537.36

import { sleep, group, check } from 'k6'
import http from 'k6/http'

export const options = {
  scenarios: {
    kube_prod: {
      tags: {
        name: 'kube_prod',
      },
      gracefulStop: '10s',
      executor: 'constant-vus',
      vus: 200,
      duration: '10s'
    },
  },
};

export default function kube_prod() {
  let response

  group('page_1 - https://prod-site.homeadvisor.com/', function () {
    response = http.get('https://prod-site.homeadvisor.com/', {
      headers: {
        Accept:
          'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'en-US,en;q=0.9',
        Connection: 'keep-alive',
        Cookie:
          '_ga=GA1.2.1683965068.1653591137; originatingSessionID=1653685517663pu020con0105E56E34F6A7D857F41BD49493B4ABB25.pu020con010-1; _gcl_au=1.1.32056815.1653685518; _fbp=fb.1.1653685518607.1977786327; AMCVS_D5C06A65527038CC0A490D4D%40AdobeOrg=1; _li_dcdm_c=.homeadvisor.com; _lc2_fpi=0f1834a81c24--01g43nd0cwqppxxwnra0rkeqqj; s_ecid=MCMID%7C00449231407847719791871621330757637558; s_evar48=Weekday; s_cc=true; s_gnr2=Repeat; s_e4=event4; cookiesEnabled=1; aff_track=2|*|23116563|*|0; _ga=GA1.3.1683965068.1653591137; c_m=undefinedType-InType-In; s_e69=Type-In; v71=%5B%5B%27Type-In%27%2C%271653685519159%27%5D%2C%5B%27Referrers%27%2C%271654800443978%27%5D%2C%5B%27Type-In%27%2C%271654870642533%27%5D%5D; check=true; s_eVar55=125158928; mbox=session#790371dada2045b0bb75e209f1ad9167#1654873070|PC#790371dada2045b0bb75e209f1ad9167.34_0#1718116010; _gid=GA1.3.361921398.1655135379; csacn=746971; s_eVar8=unknown; _gid=GA1.2.1088671492.1655236617; s_sq=%5B%5BB%5D%5D; JSESSIONID=87C3843895BA66563415A618DE1D089C.d0conpr001-1; csdcn=1655306049085; psacn=746971; psdcn=1655236616942; sess_log=1655306048966site-c8844dbff-hgkbb87C3843895BA66563415A618DE1D089C.d0conpr001-1; _uetsid=6ef2a680eb3011ec9b2a4dd7ad15f559; _uetvid=b68da980de0011ecb6ae638ed3fdc3d7; AMCV_D5C06A65527038CC0A490D4D%40AdobeOrg=1585540135%7CMCIDTS%7C19159%7CMCMID%7C00449231407847719791871621330757637558%7CMCAAMLH-1655910852%7C9%7CMCAAMB-1655910852%7CRKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y%7CMCOPTOUT-1655313252s%7CNONE%7CMCAID%7CNONE%7CvVersion%7C4.4.0; v72=%5B%5B%270%27%2C%271654611112474%27%5D%2C%5B%270%27%2C%271654611117558%27%5D%2C%5B%270%27%2C%271654792552170%27%5D%2C%5B%270%27%2C%271654800390953%27%5D%2C%5B%270%27%2C%271654870642534%27%5D%2C%5B%270%27%2C%271655135373477%27%5D%2C%5B%270%27%2C%271655152235824%27%5D%2C%5B%270%27%2C%271655236604797%27%5D%2C%5B%270%27%2C%271655236617176%27%5D%2C%5B%270%27%2C%271655306052302%27%5D%5D; s_evar46=9%3A00AM; s_dslv2=1655306052304; s_gnr=1655306052305-Repeat; s_vnum=1656648000486%26vn%3D8; s_visNum=8; s_ppv=0; v11=10.254.82.15',
        Host: 'prod-site.homeadvisor.com',
        'Sec-Fetch-Dest': 'document',
        'Sec-Fetch-Mode': 'navigate',
        'Sec-Fetch-Site': 'none',
        'Sec-Fetch-User': '?1',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent':
          'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.0.0 Safari/537.36',
        'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="102", "Google Chrome";v="102"',
        'sec-ch-ua-mobile': '?0',
        'sec-ch-ua-platform': '"macOS"',
      },
    })
    check(response, {
      'status is 200': (r) => r.status === 200,
    });
    sleep(1);
  })

}
